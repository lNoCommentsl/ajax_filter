jQuery(document).ready(function ($) {
    $('#widget_submit').click(function (e) {
        e.preventDefault();
        var data = {
            action: 'my_action',
            title: $('#title').val(),
            date: $('#date').val(),
            count: ajaxurl.c
        };
        $.ajax({
            url: ajaxurl.url,
            data: data,
            type: 'POST',
            success: function (response) {
                $('div#my_widget').empty();
                document.getElementById('my_widget').innerHTML = response;
            }
        });
    });
});
