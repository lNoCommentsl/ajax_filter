<?php

class ajax_filters_widget extends WP_Widget {

	public function __construct() {
		parent::__construct( "ajax_filters_widget", "Filters Widget",
			array( "description" => "A simple search widget to show what events soon coming" ) );

		add_action( 'wp_ajax_my_action', [ $this, 'my_action_callback' ] );
		add_action( 'wp_ajax_nopriv_my_action', [ $this, 'my_action_callback' ] );
	}


	public function form( $instance ) {

	    $count     = "";

		if ( ! empty( $instance ) ) {
			$count = $instance['count'];
		}

		$countId   = $this->get_field_id("count");
		$countName = $this->get_field_name( "count" );

		echo '<label for="' . $countId . '"> Count of events </label><br>';
		echo '<input id="' . $countId . '" type="number" min="1"  size="5" step="1" name="  ' . $countName . '" value="' . $count . '"><br>';
	}


	public function update( $newInstance, $oldInstance ) {

		$values          = array();
		$values["count"] = htmlentities( $newInstance["count"] );

		return $values;
	}

	public function my_action_callback() {

		$title = strip_tags( trim( $_POST['title'] ) );
		$date  = strip_tags( trim( $_POST['date'] ) );
		$count = strip_tags( trim( $_POST['count'] ) );
		filter_params( $title, $date, $count );

		wp_die(); // выход нужен для того, чтобы в ответе не было ничего лишнего, только то что возвращает функция
	}

	public function my_script( $instance
	) {
		$script_url = plugins_url( 'ajax_filters.js', __FILE__ );
		wp_enqueue_script( 'ajax_filter', $script_url, array( 'jquery' ) );
		wp_localize_script( 'ajax_filter', 'ajaxurl',
			array(
				'url' => admin_url( 'admin-ajax.php' ),
				'c'   => $instance['count']
			) );
	}

	public
	function widget(
		$args, $instance
	) {
		$this->my_script( $instance );
		?>
        <form method="post" action="" id="widget_form">
            <p><label for="title"><b>Title:</b></label></p>
            <input id="title" type="text" name="title">
            <p><label for="date"><b>From date:</b></label></p>
            <input id="date" type="date" name="date">
            <input id="widget_submit" type="submit" value="save">
        </form>

		<?php

		$title = trim( $_POST['title'] );
		$date  = $_POST['date'];
		$count = trim( $instance['count'] );

		echo '<div id = "my_widget" class="widget" >';
		filter_params( $title, $date, $count );
		echo '</div>';

		return $count;
	}
}

add_action( "widgets_init", function () {
	register_widget( "ajax_filters_widget" );
} );


