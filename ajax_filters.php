<?php
/*
Plugin Name: ajax filters
Plugin URI: http://wordpress.loc
Description: looking for same post
Version: 1.0
Author: NoComments
Author URI: http://wordpress.loc
License: GPLv2
*/

require_once dirname( __FILE__ ) . '/ajax_filters_widget.php';

function filter_params( $title, $date, $count ) {

	function title_filter( $title, &$wp_query ) {
		global $wpdb;
		if ( $search_term = $wp_query->get( 'search_post_title' ) ) {
			$title .= 'AND ' .  $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
		}

		return $title;
	}

	$arg = array(
		'post_type'         => 'post',
		'search_post_title' => $title,
		'post_status'       => 'publish',
		'posts_per_page'    => $count,
		'date_query'        => array(
			array(
				'after' => $date
			),
		),
	);

	add_filter( 'posts_where', 'title_filter', 10, 2 );
	$wp_query = new WP_Query( $arg );
	remove_filter( 'posts_where', 'title_filter', 10, 2 );

	if ( $wp_query->have_posts() ) {

		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();

			echo '<a href="' . get_the_permalink() . '"><p>' .
			     'Событие: ' . esc_html( get_the_title() ) . '</br>' .
			     ' Дата: ' . get_the_date() . '</p></a>';
		}
	} else {
		echo '<h2> По вашему запросу событий не найдено </h2>';
	}


}






